package com.prototyze;

import android.Manifest;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.prototyze.adapter.CardAdapter;
import com.prototyze.database.AppUsageContentProvider;
import com.prototyze.database.AppUsageSQLiteHelper;
import com.prototyze.models.AppModel;
import com.prototyze.utils.AppContentSource;
import com.prototyze.utils.Config;
import com.prototyze.utils.Prefs;
import com.prototyze.utils.loading_dialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, View.OnClickListener {
    RecyclerView mRecyclerView;
    CardAdapter mCardAdapter;
    UsageStatsManager mUsageStatsManager;
    public static String TAG = "MainActivity";
    ArrayList<AppModel> appModels;
    PackageManager packageManager;
    String[] projection;
    public static final int SPECIAL_ID = 74;
    Button updatedb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        updatedb = (Button) findViewById(R.id.updatedb);
        updatedb.setOnClickListener(this);
        packageManager = getApplicationContext().getPackageManager();
        appModels = new ArrayList<>();
        setAdapter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestPermissions();
    }

    private void getContentAppUsageData() {
        getSupportLoaderManager().initLoader(SPECIAL_ID,
                new Bundle(), this);
    }

    private void setAdapter() {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this, 2));
        mCardAdapter = new CardAdapter(getApplicationContext());
        mRecyclerView.setAdapter(mCardAdapter);
        mCardAdapter.addData(appModels);
    }

    private void requestPermissions() {
//        if (getPackageManager().checkPermission(Manifest.permission.PACKAGE_USAGE_STATS, getPackageName()) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(MainActivity.this,
//                    new String[]{Manifest.permission.PACKAGE_USAGE_STATS},
//                    102);
        initManager();
        //    }
        //    Log.d("permissions", "" + (getPackageManager().checkPermission(Manifest.permission.PACKAGE_USAGE_STATS, getPackageName()) != PackageManager.PERMISSION_GRANTED));
    }


    private void initManager() {
        mUsageStatsManager = (UsageStatsManager) getApplicationContext().getSystemService(Context.USAGE_STATS_SERVICE);
        Map<String, UsageStats> usageStatsList = null;
        try {
            usageStatsList = getUsageStatistics();
            Log.d("usagestats", "" + usageStatsList.size());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Boolean isDataStored = Prefs.with(getApplicationContext()).getBoolean("is_app_stored", false);
        if (usageStatsList.size() == 0) {
            Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
            startActivity(intent);
            Prefs.with(getApplicationContext()).save("usage_permission", false);
            Toast.makeText(getApplicationContext(), "Please provide permissions !", Toast.LENGTH_LONG).show();
        } else if (isDataStored) {
            Prefs.with(getApplicationContext()).save("is_app_stored", true);
            Prefs.with(getApplicationContext()).save("usage_permission", true);
            getContentAppUsageData();
        } else {
            Prefs.with(getApplicationContext()).save("is_app_stored", false);
            Prefs.with(getApplicationContext()).save("usage_permission", true);
            storeInstalledApps();
        }
    }

    public Map<String, UsageStats> getUsageStatistics() throws PackageManager.NameNotFoundException {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_WEEK, -1);
        //Log.d("tagged", "" + cal.toString());
        Map<String, UsageStats> queryUsageStats = mUsageStatsManager.queryAndAggregateUsageStats(cal.getTimeInMillis(), System.currentTimeMillis());
        return queryUsageStats;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 102: {

            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        loading_dialog.loading_box(MainActivity.this);
        projection = new String[]{AppUsageSQLiteHelper.APPNAME, AppUsageSQLiteHelper.APPPACKAGENAME, AppUsageSQLiteHelper.APPUSSAGE};
        return new CursorLoader(getApplicationContext(), Config.CONTENT_URI, projection, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        appModels.clear();
        Log.d("before",""+appModels.size());
        if (!data.isAfterLast()) {
            for (data.moveToFirst(); !data.isLast(); data.moveToNext()) {
                AppModel appmodel = new AppModel();
                appmodel.setName(data.getString(data.getColumnIndex(projection[0])));
                appmodel.setPackageName(data.getString(data.getColumnIndex(projection[1])));
                appmodel.setUsageTime(data.getString(data.getColumnIndex(projection[2])));
                appModels.add(appmodel);
            }
        }
        Log.d("after",""+appModels.size());
        if (mCardAdapter != null)
            mCardAdapter.addData(appModels);
        loading_dialog.loading_box_stop();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public void storeInstalledApps() {
        new AsyncTask<Void, Integer, String>() {
            protected void onPreExecute() {
                loading_dialog.loading_box(MainActivity.this);
            }

            @Override
            protected String doInBackground(Void... voids) {
                List<PackageInfo> packs = packageManager.getInstalledPackages(0);
                for (int i = 0; i < packs.size(); i++) {
                    PackageInfo p = packs.get(i);
                    AppModel appModel = new AppModel();
                    appModel.setName(p.applicationInfo.loadLabel(packageManager).toString());
                    appModel.setPackageName(p.packageName);
                    appModel.setUsageTime("0");
                    addAppModels(appModel);
                }
                updateUsageStats();
                return null;
            }

            protected void onPostExecute(String result) {
                loading_dialog.loading_box_stop();
                Prefs.with(getApplicationContext()).save("is_app_stored", true);
                getContentAppUsageData();
            }
        }.execute();
    }

    public void addAppModels(AppModel appModel) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppUsageSQLiteHelper.APPNAME, appModel.name);
        contentValues.put(AppUsageSQLiteHelper.APPPACKAGENAME, appModel.packageName);
        contentValues.put(AppUsageSQLiteHelper.APPUSSAGE, "" + appModel.usageTime);
        Uri uri = getContentResolver().insert(Config.CONTENT_URI, contentValues);
        // Log.d("ur_tagged", "" + uri + "  " + appModel.packageName);
    }

    public void updateUsageStats() {
        try {
            Map<String, UsageStats> usageStatsList = getUsageStatistics();
            Set<String> strings = usageStatsList.keySet();
            String[] statMap = strings.toArray(new String[strings.size()]);
            for (int i = 0; i < statMap.length; i++) {
                UsageStats usageStats = usageStatsList.get(statMap[i]);
                ContentValues contentValues = new ContentValues();
                contentValues.put(AppUsageSQLiteHelper.APPUSSAGE, (usageStats.getLastTimeUsed() / (60 * 1000 * 60)));
                getContentResolver().update(Config.CONTENT_URI, contentValues, AppUsageSQLiteHelper.APPPACKAGENAME + " =  '" + statMap[i] + "'", null);
                //   Log.d("dskjdskjksjn", AppUsageSQLiteHelper.APPPACKAGENAME + " =  " + statMap[i] + "  " + (usageStats.getTotalTimeInForeground()) / (60 * 1000 * 60));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.updatedb:
                new AsyncTask<Void, Integer, String>() {
                    protected void onPreExecute() {
                        loading_dialog.loading_box(MainActivity.this);
                    }

                    @Override
                    protected String doInBackground(Void... voids) {
                        updateUsageStats();
                        return null;
                    }

                    protected void onPostExecute(String result) {
                        loading_dialog.loading_box_stop();
                        getContentAppUsageData();
                    }
                }.execute();
                break;
        }
    }
}
