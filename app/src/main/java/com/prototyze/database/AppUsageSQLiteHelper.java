package com.prototyze.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

/**
 * Created by pranavvij on 02/11/16.
 */

public class AppUsageSQLiteHelper extends SQLiteOpenHelper {
    public static String DATABASENAME = "AppUsage.db";
    public static String TABLE_NAME = "appUsageDB";
    public static int TABLEVERSION = 1;
    public static String APPNAME = "appName";
    public static String APPPACKAGENAME = "appImagePackageName";
    public static String APPUSSAGE = "appusage";
    public static String APPID = "appId";

    private String sqlCreate = "CREATE TABLE " + TABLE_NAME + "( " +
            APPID + " INTEGER PRIMARY KEY AUTOINCREMENT , " +
            APPNAME + " TEXT ," +
            APPPACKAGENAME + " TEXT ," +
            APPUSSAGE + " TEXT );";

    public AppUsageSQLiteHelper(Context context) {
        super(context, DATABASENAME, null, TABLEVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(sqlCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

}
