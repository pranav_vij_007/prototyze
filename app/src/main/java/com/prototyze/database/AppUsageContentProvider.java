package com.prototyze.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.Nullable;

/**
 * Created by pranavvij on 02/11/16.
 */

public class AppUsageContentProvider extends ContentProvider {
    AppUsageDataSource appUsageDataSource;
    Context context;

    public static final String AUTHORITY = "com.prototyze.appussage";
    private static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/appusage");

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public boolean onCreate() {
        context = getContext();
        appUsageDataSource = new AppUsageDataSource(context);
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String id = null;
        return appUsageDataSource.getAppModels(id, projection, selection, selectionArgs, sortOrder);
    }


    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        try {
            long id = appUsageDataSource.insertAppContent(values);
            Uri returnUri = ContentUris.withAppendedId(CONTENT_URI, id);
            return returnUri;
        } catch(Exception e) {
            return null;
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        String id = null;
        return appUsageDataSource.deleteApp(id);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        String id = null;
        return appUsageDataSource.updateApp(id,values,selection);
    }
}