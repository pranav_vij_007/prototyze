package com.prototyze.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

import com.prototyze.models.AppModel;

import java.util.ArrayList;

/**
 * Created by pranavvij on 02/11/16.
 */

public class AppUsageDataSource {
    AppUsageSQLiteHelper appUsageSQLiteHelper;
    SQLiteDatabase sqLiteOpenHelper;

    public AppUsageDataSource(Context context) {
        appUsageSQLiteHelper = new AppUsageSQLiteHelper(context);
        sqLiteOpenHelper = appUsageSQLiteHelper.getWritableDatabase();
    }

    public long insertAppContent(ContentValues contentValues) throws SQLException {
        long id = sqLiteOpenHelper.insert(AppUsageSQLiteHelper.TABLE_NAME, null, contentValues);
        if(id <=0 ) {
            throw new SQLException("Failed to add a row");
        }

        return id;
    }

    public Cursor getAppModels(String id, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder sqliteQueryBuilder = new SQLiteQueryBuilder();
        sqliteQueryBuilder.setTables(AppUsageSQLiteHelper.TABLE_NAME);

        if (id != null) {
            sqliteQueryBuilder.appendWhere(AppUsageSQLiteHelper.APPID + " = " + id);
        }

        if (sortOrder == null || sortOrder == "") {
            sortOrder = AppUsageSQLiteHelper.APPID;
        }

        Cursor cursor = sqliteQueryBuilder.query(appUsageSQLiteHelper.getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);
        return cursor;
    }




    public int deleteApp(String id) {
        if (id == null) {
            return sqLiteOpenHelper.delete(appUsageSQLiteHelper.TABLE_NAME, null, null);
        } else {
            return sqLiteOpenHelper.delete(appUsageSQLiteHelper.TABLE_NAME, AppUsageSQLiteHelper.APPID + "=?", new String[]{id});
        }
    }

    public int updateApp(String id, ContentValues values,String selection) {
        if (id == null) {
            return sqLiteOpenHelper.update(AppUsageSQLiteHelper.TABLE_NAME, values, selection, null);
        } else {
            return sqLiteOpenHelper.update(AppUsageSQLiteHelper.TABLE_NAME, values,selection, null);
        }
    }
}
