package com.prototyze.models;

import android.app.usage.UsageStats;
import android.graphics.drawable.Drawable;

/**
 * Created by pranavvij on 01/11/16.
 */

public class AppModel {
    public String name,packageName,usageTime;

    public void setName(String name) {
        this.name = name;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public void setUsageTime(String usageTime) {
        this.usageTime = usageTime;
    }

    public String getName() {
        return name;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getUsageTime() {
        return usageTime;
    }
}
