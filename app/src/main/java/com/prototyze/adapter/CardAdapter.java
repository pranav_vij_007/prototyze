package com.prototyze.adapter;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prototyze.R;
import com.prototyze.models.AppModel;

import java.util.ArrayList;
import java.util.List;


public class CardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<AppModel> mItems;
    Context context;

    public CardAdapter(Context context) {
        super();
        this.context = context;
        mItems = new ArrayList<AppModel>();
    }

    public void addData(ArrayList<AppModel> mItems) {
        clear();
        this.mItems.addAll(mItems);
        notifyDataSetChanged();
    }

    public void clear() {
        mItems.clear();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder1, int position) {
        AppModel appModel = mItems.get(position);
        ViewHolder viewHolder = (CardAdapter.ViewHolder) viewHolder1;
        viewHolder.appname.setText(appModel.getName());
        viewHolder.usageTime.setText(appModel.getUsageTime());
        viewHolder.imagesrc.setImageDrawable(getAppIcon(appModel.getPackageName()));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imagesrc;
        public RelativeLayout squareRelativeLayout;
        public TextView appname, usageTime;

        public ViewHolder(View itemView) {
            super(itemView);
            imagesrc = (ImageView) itemView.findViewById(R.id.imagesrc);
            squareRelativeLayout = (RelativeLayout) itemView.findViewById(R.id.squareRelativeLayout);
            appname = (TextView) itemView.findViewById(R.id.appname);
            usageTime = (TextView) itemView.findViewById(R.id.usageTime);
        }
    }

    public Drawable getAppIcon(String packageName) {
        try {
            return context.getPackageManager()
                    .getApplicationIcon(packageName);
        } catch (PackageManager.NameNotFoundException e) {
            return context
                    .getDrawable(R.drawable.ic_default_app_launcher);
        }
    }
}
