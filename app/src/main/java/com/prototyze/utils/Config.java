package com.prototyze.utils;

import android.net.Uri;

/**
 * Created by cl-macmini81 on 11/3/16.
 */

public class Config {
    public static final String AUTHORITY = "com.prototyze.appussage";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/appusage");
}
