package com.prototyze.utils;

import android.app.ProgressDialog;
import android.content.Context;

import com.prototyze.R;

/**
 * Created by pranavvij on 03/11/16.
 */

public class loading_dialog {
    private static ProgressDialog progressDialog = null;

    public static void loading_box(Context c) {
        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(c,
                        android.R.style.Theme_Translucent_NoTitleBar);
                progressDialog.show();
                progressDialog.setCancelable(false);

                progressDialog.setContentView(R.layout.loading_box);

            } else {
                if (!progressDialog.isShowing())
                    progressDialog.show();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /**
     * stop loading view
     */
    public static void loading_box_stop() {
        try {
            if (progressDialog != null)
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
